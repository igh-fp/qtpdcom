/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <QtTest/QtTest>

class MessageModelTest:
    public QObject
{
    Q_OBJECT

    private slots:
        void test_plain_xml();
        void test_message_manager();
        void test_mixed();
        void test_history();
        void test_all_historic();
        void test_can_fetch_more();
        void test_history_then_live();
        void test_sorting();
};

/****************************************************************************/
