/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *               2022 Florian Pose (fp at igh dot de),
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PDCOM5_EXCEPTION_H
#define PDCOM5_EXCEPTION_H

#include <stdexcept>
#include <string>

namespace PdCom {

struct Exception : std::runtime_error
{
    using std::runtime_error::runtime_error;
};

struct InternalError : Exception
{
    InternalError() : Exception("Internal error, please file a bug report") {}
    InternalError(const std::string &msg) :
        Exception("Internal error, please file a bug report: " + msg)
    {}
};

struct InvalidArgument : Exception
{
    using Exception::Exception;
};

struct ConnectionFailed : Exception
{
    ConnectionFailed() : Exception("Connection failed") {}
};

struct EmptyVariable : Exception
{
    EmptyVariable() : Exception("Variable is empty") {}
};

struct InvalidSubscription : Exception
{
    InvalidSubscription() : Exception("invalid subscription") {}
};

struct LoginRequired : Exception
{
    LoginRequired() : Exception("Login is required") {}
};

struct NotConnected : Exception
{
    NotConnected() : Exception("Not connected") {}
};

struct ProcessGoneAway : Exception
{
    ProcessGoneAway() : Exception("Process has gone away") {}
};

struct ProtocolError : Exception
{
    using Exception::Exception;
};

struct ReadFailure : Exception
{
    int errno_;
    ReadFailure(int e) :
        Exception("Read failure, errno: " + std::to_string(e)), errno_(e)
    {}
};

struct TlsError : Exception
{
    TlsError(std::string what, int err_code) :
        Exception(std::move(what)), err_code_(err_code)
    {}
    int err_code_;
};

struct WriteFailure : Exception
{
    int errno_;
    using Exception::Exception;
    WriteFailure(int e) :
        Exception("Write failure, errno: " + std::to_string(e)), errno_(e)
    {}
};

}  // namespace PdCom

#endif  // PDCOM5_EXCEPTION_H
