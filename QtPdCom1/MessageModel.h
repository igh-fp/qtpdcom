/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_MESSAGEMODEL_H
#define QTPDCOM_MESSAGEMODEL_H

#include "Message.h"

#include <QAbstractTableModel>
#include <QIcon>

#include <memory>

namespace QtPdCom {

class Process;

/****************************************************************************/

/** List of Messages.
 *
 * \see Message.
 */
class QTPDCOM_PUBLIC MessageModel:
    public QAbstractTableModel
{
    Q_OBJECT

    friend class Message::Impl;

    public:
        MessageModel(QObject *parent = nullptr);
        ~MessageModel();

        void load(const QString &, const QString & = QString(),
                const QString & = QString());
        void clear();

        void setRowLimit(int);
        int getRowLimit() const;

        void connect(QtPdCom::Process *);
        Q_INVOKABLE void translate(const QString &);

        void setIcon(Message::Type, const QIcon &);
        const QIcon &getIcon(Message::Type) const;
        void setIconPath(Message::Type, const QString &);

	enum Roles  {
	    DecorationPathRole = Qt::UserRole + 1,
	    TimeStringRole = Qt::UserRole + 2,
	    ResetTimeStringRole = Qt::UserRole + 3,
	    MessageTypeRole = Qt::UserRole + 4,
	};
	Q_ENUM(Roles)

        // from QAbstractItemModel
        virtual int rowCount(const QModelIndex &) const override;
        virtual int columnCount(const QModelIndex &) const override;
        virtual QVariant data(const QModelIndex &, int) const override;
        virtual QVariant headerData(int, Qt::Orientation, int) const override;
        virtual Qt::ItemFlags flags(const QModelIndex &) const override;
        virtual QHash<int, QByteArray> roleNames() const override;
        virtual bool canFetchMore(const QModelIndex &) const override;
        virtual void fetchMore(const QModelIndex &) override;

        /** Exception type.
         */
        struct Exception {
            /** Constructor.
             */
            Exception(const QString &msg): msg(msg) {}
            QString msg; /**< Exception message. */
        };

    signals:
        /** Emitted, when a new message gets active.
         *
         * This signal announces the most recent message. It is only emitted
         * for the first message getting active, or for a subsequent message
         * with a higher type.
         *
         * \param message The message that got active. The signal is emitted
         *                with \a message being \a NULL, if no messages are
         *                active any more.
         */
        void currentMessage(const QtPdCom::Message *message);

        /** Emitted, when a new message gets active.
         *
         * This signal announces any new arriving message.
         *
         * \param message The message that got active.
         */
        void anyMessage(const QtPdCom::Message *message);

    protected:
        bool event(QEvent *) override;

    private:
        class Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

} // namespace

#endif
