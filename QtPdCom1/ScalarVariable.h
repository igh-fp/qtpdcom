/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_SCALARVARIABLE_H
#define QTPDCOM_SCALARVARIABLE_H

#include "ScalarSubscriber.h"
#include "Export.h"

#include <QObject>

namespace QtPdCom {

/****************************************************************************/

/** Abstract Scalar Value.
 */
class QTPDCOM_PUBLIC AbstractScalarVariable:
    public QObject, public ScalarSubscriber
{
    Q_OBJECT

    public:
        using QObject::QObject;

        struct Exception {
            /** Constructor.
             */
            Exception(const QString &msg):
                msg(msg) {}
            QString msg; /**< Exception message. */
        };

        template <typename T>
        typename std::enable_if<std::is_arithmetic<T>::value, void>::type
        copyData(T &dest) const
        {
            PdCom::details::copyData(
                    &dest, PdCom::details::TypeInfoTraits<T>::type_info.type,
                    getData(),
                    getVariable().getTypeInfo().type, 1);
        }

    signals:
        void valueChanged(); /**< Emitted, when the value changes, or the
                               variable is disconnected. */
};

/****************************************************************************/

/** Scalar Value Template.
 */
template <class T>
class ScalarVariable:
    public AbstractScalarVariable
{
    public:
        ScalarVariable(QObject *parent = nullptr);
        virtual ~ScalarVariable();

        void clearData(); // pure-virtual from ScalarSubscriber
        bool hasData() const;

        T getValue() const;
        std::chrono::nanoseconds getMTime() const;
        void inc();

    private:
        T value; /**< Current value. */
        /** Modification Time of Current value. */
        std::chrono::nanoseconds mTime;
        bool dataPresent; /**< There is a process value to display. */

        // pure-virtual from ScalarSubscriber
        void newValues(std::chrono::nanoseconds) override;
};

/****************************************************************************/

typedef ScalarVariable<bool> BoolVariable;
typedef ScalarVariable<int> IntVariable;
typedef ScalarVariable<double> DoubleVariable;

/****************************************************************************/

/** Constructor.
 */
template <class T>
ScalarVariable<T>::ScalarVariable(QObject *parent):
    AbstractScalarVariable(parent),
    value((T) 0),
    dataPresent(false)
{
}

/****************************************************************************/

/** Destructor.
 */
template <class T>
ScalarVariable<T>::~ScalarVariable()
{
}

/****************************************************************************/

template <class T>
void ScalarVariable<T>::clearData()
{
    value = ((T) 0);
    dataPresent = false;
    emit valueChanged();
}

/****************************************************************************/

/**
 * \return \a true, if data are present.
 */
template <class T>
inline bool ScalarVariable<T>::hasData() const
{
    return dataPresent;
}

/****************************************************************************/

/**
 * \return The current #value.
 */
template <class T>
inline T ScalarVariable<T>::getValue() const
{
    return value;
}

/****************************************************************************/

/**
 * \return The current Modification Time.
 */
template <class T>
inline std::chrono::nanoseconds
ScalarVariable<T>::getMTime() const
{
    return mTime;
}

/****************************************************************************/

/** Increments the current #value and writes it to the process.
 *
 * This does \a not update #value directly.
 */
template <class T>
void ScalarVariable<T>::inc()
{
    writeValue(value + 1);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
template <class T>
void ScalarVariable<T>::newValues(std::chrono::nanoseconds ts)
{
    T newValue;

    copyData(newValue);
    newValue = newValue * scale + offset;
    mTime = std::chrono::nanoseconds(ts);

    if (newValue != value || !dataPresent) {
        value = newValue;
        dataPresent = true;
        emit valueChanged();
    }
}

/****************************************************************************/

} // namespace

#endif
