/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_TABLEMODEL_H
#define QTPDCOM_TABLEMODEL_H

#include "TableColumn.h"
#include "ScalarVariable.h"

#include <memory>
#include <QAbstractTableModel>
#include <QColor>

namespace QtPdCom {

/****************************************************************************/

/** Table model.
 *
 * \see TableColumn.
 */
class QTPDCOM_PUBLIC TableModel:
    public QAbstractTableModel
{
    Q_OBJECT

    public:
        TableModel(QObject *parent = nullptr);
        ~TableModel();

        void addColumn(TableColumn *);
        void clearColumns();

        bool isEditing() const;
        unsigned int getRowCapacity() const;
        bool hasVisibleRowsVariable() const;
        virtual int rowCount(const QModelIndex &) const;
        virtual int columnCount(const QModelIndex &) const;
        virtual QVariant data(const QModelIndex &, int) const;
        virtual QVariant headerData(int, Qt::Orientation, int) const;
        virtual Qt::ItemFlags flags(const QModelIndex &) const;
        virtual bool setData(const QModelIndex &, const QVariant &, int);

        /** Subscribe to a process variable for highlighting a row.
          The variable value determines the highlighted row.
         */
        void setHighlightRowVariable(
                PdCom::Variable, /**< Process variable. */
                const PdCom::Selector & = {}, /**< Selector. */
                const Transmission & = QtPdCom::event_mode /**< Transmission. */
                );
        /** Subscribe to a process variable for highlighting a row.
          The variable value determines the highlighted row.
         */
        void setHighlightRowVariable(
                PdCom::Process *, /**< Process variable. */
                const QString &, /**< Variable path. */
                const PdCom::Selector & = {}, /**< Selector. */
                const Transmission & = QtPdCom::event_mode /**< Transmission. */
                );

        void clearHighlightRowVariable();

        /** Subscribe to a process variable for setting the visible rows
            of the Table, this is most often a parameter which also gets
            updated if the Table row count gets changed by editing.
         */
        void setVisibleRowsVariable(
                PdCom::Variable pv /**< Process variable. */
                );

        void clearVisibleRowsVariable();

        void setHighlightColor(QColor, int = -1);
        void setDisabledColor(QColor, int = -1);

        /** Exception type.
         */
        struct Exception {
            /** Constructor.
             */
            Exception(const QString &msg): msg(msg) {}
            QString msg; /**< Exception message. */
        };

    signals:
        void editingChanged(bool);

    public slots:
        void commit();
        void revert(); // virtual from AbstractItemModel
        void addRow();
        void remRow(); // not to be mixed up with the removeRow()
                       // function from AbstractItemModel

    private:
        class Q_DECL_HIDDEN Impl;

        std::unique_ptr<Impl> impl;

    private slots:
        void dimensionChanged();
        void columnHeaderChanged();
        void valueChanged();
        void highlightRowChanged();
        void visibleRowCountChanged();
};

/****************************************************************************/

} // namespace

#endif
