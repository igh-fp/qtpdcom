/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_TRANSMISSION_H
#define QTPDCOM_TRANSMISSION_H

#include <pdcom5/Subscriber.h> // PdCom::Transmission

#include "Export.h"

#include <chrono>
#include <stdexcept>

namespace QtPdCom {

/****************************************************************************/

/// Tag for event-based subscription.
constexpr struct event_mode_tag
{
} event_mode;

/// Tag for poll-based subscription.
constexpr struct poll_mode_tag
{
} poll_mode;

class QTPDCOM_PUBLIC Poll
{
    public:
        template <typename T, typename R>
        constexpr Poll(std::chrono::duration<T, R> d):
            interval_(checkInterval(
                        std::chrono::duration_cast<std::chrono::duration<double>>(d)))
        {}

        constexpr std::chrono::duration<double> getInterval() const {
            return interval_;
        }

    private:
        std::chrono::duration<double> interval_;

        static constexpr std::chrono::duration<double> checkInterval(
                std::chrono::duration<double> d)
        {
            return d.count() <= 0 ? throw std::invalid_argument(
                    "Poll period must be greater than zero")
                : d;
        }
};

/** Transmission mode for subscriptions.
 *
 * This class specifies whether a subscription should be updated periodically,
 * event-based or by polling only.
 *
 */
class QTPDCOM_PUBLIC Transmission
{

  public:
    constexpr double getInterval() const noexcept { return interval_; }

    template <typename T, typename R>
    constexpr Transmission(std::chrono::duration<T, R> d) :
        mode_(Continuous),
        interval_(checkInterval(
                std::chrono::duration_cast<std::chrono::duration<double>>(d)
                        .count()))
    {}

    constexpr Transmission(event_mode_tag) noexcept :
        mode_(Event), interval_(0.0) {}

    constexpr Transmission(poll_mode_tag, double interval) :
        mode_(Poll), interval_(checkInterval(interval)) {}

    constexpr Transmission(const Poll &poll):
        mode_(Poll), interval_(poll.getInterval().count()) {}

    bool operator==(const Transmission &o) const noexcept
    {
        return o.interval_ == interval_ and o.mode_ == mode_;
    }

    constexpr bool isContinuous() const
    {
        return mode_ == Continuous and interval_ > 0.0;
    }

    constexpr bool isPoll() const
    {
        return mode_ == Poll and interval_ > 0.0;
    }

    PdCom::Transmission toPdCom() const;
    QString toString() const;

  private:
    enum {
        Poll = -1,
        Event,
        Continuous
    } mode_;

    double interval_;

    static constexpr double checkInterval(double d)
    {
        return d <= 0 ? throw std::invalid_argument(
                       "Interval must be greater than zero")
                      : d;
    }

};

/****************************************************************************/

} // namespace

#endif
