/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageManager.h"

#include <QDebug>

using QtPdCom::MessageManager;

/****************************************************************************/

MessageManager::MessageManager()
{
}

/****************************************************************************/

MessageManager::~MessageManager()
{
    reset();
}

/****************************************************************************/

void MessageManager::reset()
{
    emit processResetSignal();
}

/****************************************************************************/

void MessageManager::processMessage(PdCom::Message message)
{
    emit processMessageSignal(message);
}

/****************************************************************************/

void MessageManager::getMessageReply(PdCom::Message message)
{
    emit getMessageReplySignal(message);
}

/****************************************************************************/

void MessageManager::activeMessagesReply(
        std::vector<PdCom::Message> messageList)
{
    emit activeMessagesReplySignal(messageList);
}

/****************************************************************************/
