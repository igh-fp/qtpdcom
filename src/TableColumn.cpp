/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableColumn.h"
using QtPdCom::TableColumn;

#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

#include <QDebug>
#include <QVariant>
#include <QBrush>

#define DEFAULT_DECIMALS        15
#define DEFAULT_HIGHLIGHT_COLOR QColor(152, 183, 255)
#define DEFAULT_DISABLED_COLOR  QColor(220, 220, 220)

/****************************************************************************/

class QtPdCom::TableColumn::Impl
{
    friend class TableColumn;

    public:
        Impl(TableColumn *parent, const QString &header):
            parent(parent),
            header(header),
            scale(1.0),
            offset(0.0),
            dataPresent(false),
            editData(NULL),
            enabled(true),
            highlightRow(-1),
            decimals(DEFAULT_DECIMALS),
            highlightColor(DEFAULT_HIGHLIGHT_COLOR),
            disabledColor(DEFAULT_DISABLED_COLOR)
        {
        }

        ~Impl()
        {
            if (editData) {
                delete [] editData;
            }
        }

        void stateChanged(PdCom::Subscription::State state)
        {
            if (state == PdCom::Subscription::State::Active) {
                emit parent->dimensionChanged();
            }

            if (state != PdCom::Subscription::State::Active) {
                dataPresent = false;
                if (editData) {
                    delete [] editData;
                    editData = NULL;
                }
                emit parent->dimensionChanged();
            }
        }

        void newValues(std::chrono::nanoseconds)
        {
            dataPresent = true;
            emit parent->valueChanged();
        }

    private:
        TableColumn *parent; /**< Parent object */
        QString header; /**< Table column header. */

        double scale; /**< Scale factor for values. */
        double offset; /**< Offset for values. */
        bool dataPresent; /**< Valid data have been received. */
        double *editData; /**< Temporary editing data. */
        bool enabled; /**< Column enabled. */
        QHash<unsigned int, bool> enabledRows; /**< Enabled table rows. */
        int highlightRow; /**< Index of the row to highlight, or -1. */
        quint32 decimals; /**< Number of decimal digits. */

        QColor highlightColor;
        QColor disabledColor;

        class Subscription;
        std::unique_ptr<Subscription> subscription;
};

/****************************************************************************/

class QtPdCom::TableColumn::Impl::Subscription:
    public PdCom::Subscriber, PdCom::Subscription
{
    friend class TableColumn;

    public:
        Subscription(TableColumn::Impl *impl, PdCom::Variable pv,
                const Transmission &transmission):
            Subscriber(transmission.toPdCom()),
            PdCom::Subscription(*this, pv),
            impl(impl)
        {
        }

        Subscription(TableColumn::Impl *impl, PdCom::Process *process,
                const std::string &path, const Transmission &transmission):
            Subscriber(transmission.toPdCom()),
            PdCom::Subscription(*this, *process, path),
            impl(impl)
        {
        }

        void copyData(double *dest, size_t nelem)
        {
            PdCom::details::copyData(dest,
                    PdCom::details::TypeInfoTraits<double>::type_info.type,
                    getData(), getVariable().getTypeInfo().type, nelem);
        }

    private:
        TableColumn::Impl *impl;

        void stateChanged(const PdCom::Subscription &) override
        {
            if (getState() == PdCom::Subscription::State::Active) {
                // changed to active. If event mode, poll once.
                if (getTransmission() == PdCom::event_mode) {
                    poll(); // poll once to get initial value
                }
            }

            impl->stateChanged(getState());
        }

        void newValues(std::chrono::nanoseconds ts) override
        {
            impl->newValues(ts);
        }
};

/****************************************************************************/

/** Constructor.
 */
TableColumn::TableColumn(const QString &header, QObject *parent):
    QObject(parent),
    impl(std::unique_ptr<TableColumn::Impl>(new Impl(this, header)))
{
}

/****************************************************************************/

/** Destructor.
 */
TableColumn::~TableColumn()
{
}

/****************************************************************************/

/** Sets the column header.
 */
void TableColumn::setHeader(const QString &h)
{
    impl->header = h;

    emit headerChanged();
}

/****************************************************************************/

const QString &TableColumn::getHeader() const
{
    return impl->header;
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void TableColumn::setVariable(
        PdCom::Variable pv,
        const Transmission &transmission,
        double scale,
        double offset
        )
{
    clearVariable();

    if (pv.empty()) {
        return;
    }

    impl->scale = scale;
    impl->offset = offset;

    try {
        impl->subscription =
            std::unique_ptr<Impl::Subscription>(new Impl::Subscription(
                        impl.get(), pv, transmission));
    } catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                " \"%1\" with transmission %2: %3")
            .arg(QString(pv.getPath().c_str()))
            .arg(transmission.toString())
            .arg(e.what());
        return;
    }

    emit dimensionChanged();
    emit valueChanged();
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void TableColumn::setVariable(
        PdCom::Process *process,
        const QString &path,
        const Transmission &transmission,
        double scale,
        double offset
        )
{
    clearVariable();

    if (path.isEmpty() or not process) {
        return;
    }

    impl->scale = scale;
    impl->offset = offset;

    try {
        impl->subscription =
            std::unique_ptr<Impl::Subscription>(new Impl::Subscription(
                        impl.get(), process, path.toLocal8Bit().constData(),
                        transmission));
    } catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                " \"%1\" with transmission %2: %3")
            .arg(path)
            .arg(transmission.toString())
            .arg(e.what());
        return;
    }

    emit dimensionChanged();
    emit valueChanged();
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void TableColumn::clearVariable()
{
    if (impl->subscription) {
        impl->subscription.reset();
        impl->dataPresent = false;
        if (impl->editData) {
            delete [] impl->editData;
            impl->editData = NULL;
        }
        impl->stateChanged(PdCom::Subscription::State::Invalid);
        emit dimensionChanged();
        emit valueChanged();
    }
}

/****************************************************************************/

void TableColumn::clearData()
{
    impl->dataPresent = false;
    emit valueChanged();
}

/****************************************************************************/

/** Sets the number of #decimals.
 */
void TableColumn::setDecimals(quint32 value)
{
    if (value != impl->decimals) {
        impl->decimals = value;
        emit valueChanged();
    }
}

/****************************************************************************/

/** Get number of rows.
 */
unsigned int TableColumn::getRows() const
{
    if (impl->subscription and !impl->subscription->getVariable().empty()) {
        PdCom::Variable pv(impl->subscription->getVariable());
        return pv.getSizeInfo().totalElements();
    } else {
        return 0U;
    }
}

/****************************************************************************/

/** Get display text.
 */
QVariant TableColumn::data(unsigned int row, int role) const
{
    switch (role) {

        case Qt::DisplayRole:
        case Qt::EditRole:
            if (impl->subscription and
                    !impl->subscription->getVariable().empty() and
                    impl->dataPresent) {
                PdCom::Variable pv(impl->subscription->getVariable());
                unsigned int nelem = pv.getSizeInfo().totalElements();
                if (row < nelem) {
                    double val;

                    if (impl->editData) {
                        val = impl->editData[row];
                    } else {
                        double v[nelem];
                        impl->subscription->copyData(v, nelem); // FIXME one
                        val = v[row] * impl->scale + impl->offset;
                    }

                    return QLocale().toString(val, 'f', impl->decimals);

                } else {
                    return "";
                }
            } else {
                return "";
            }
            break;

        case Qt::BackgroundRole:
            if (impl->subscription and
                    !impl->subscription->getVariable().empty() and
                    impl->dataPresent) {
                PdCom::Variable pv(impl->subscription->getVariable());
                unsigned int nelem = pv.getSizeInfo().totalElements();

                if (!impl->enabled || !impl->enabledRows.value(row, true)) {
                    // FIXME also, if variable is not writable.
                    return QBrush(impl->disabledColor);
                } else if (impl->editData) {
                    return QBrush(Qt::yellow);
                } else if ((int) row == impl->highlightRow) {
                    return QBrush(impl->highlightColor);
                } else if (row >= nelem) {
                    return QBrush(Qt::darkGray);
                }
            }
            return QBrush();

        default:
            return QVariant();
    }
}

/****************************************************************************/

/** Get header data.
 */
QVariant TableColumn::headerData(int role) const
{
    switch (role) {
        case Qt::DisplayRole:
            return impl->header;

        default:
            return QVariant();
    }
}

/****************************************************************************/

/** Implements the Model interface.
 */
Qt::ItemFlags TableColumn::flags(unsigned int row) const
{
    Qt::ItemFlags f = Qt::ItemFlags();

    if (!impl->subscription or impl->subscription->getVariable().empty()
            or !impl->dataPresent) {
        return f;
    }

    f |= Qt::ItemIsEnabled;

    if (impl->enabled && impl->enabledRows.value(row, true)) {
        // FIXME only if variable is writable
        f |= Qt::ItemIsEditable;
    }

    return f;
}

/****************************************************************************/

/** Set an edit value.
 */
bool TableColumn::setData(
        unsigned int row,
        const QString &valueString,
        int role
        )
{
    Q_UNUSED(role);

    bool ok;
    double value = QLocale().toDouble(valueString, &ok);

    if (!impl->subscription or impl->subscription->getVariable().empty()
            or !impl->dataPresent or !ok) {
        qCritical() << "Failed to edit variable";
        return false;
    }

    PdCom::Variable pv(impl->subscription->getVariable());
    auto nelem(pv.getSizeInfo().totalElements());

    if (row >= nelem) {
        qCritical() << "row" << row << "does not exist";
        return false;
    }

    double data[nelem];
    impl->subscription->copyData(data, nelem);
    for (size_t i = 0; i < nelem; i++) {
        data[i] = data[i] * impl->scale + impl->offset;
    }

    if (!impl->editData) {
        impl->editData = new double[nelem];
        impl->subscription->copyData(impl->editData, nelem);
        for (size_t i = 0; i < nelem; i++) {
            impl->editData[i] =
                impl->editData[i] * impl->scale + impl->offset;
        }
    }

    impl->editData[row] = value;

    for (size_t i = 0; i < nelem; i++) {
        if (data[i] != impl->editData[i]) {
            // data differ from process
            return true;
        }
    }

    // data are equal to process (again)
    delete [] impl->editData;
    impl->editData = NULL;

    return true;
}

/****************************************************************************/

/** Set enabled for a column
 */
void TableColumn::setEnabled(bool value, int row)
{
    if (row < 0) {
        impl->enabled = value;
    } else {
        impl->enabledRows.insert(row, value);
    }

    emit valueChanged(); // FIXME, gibt es auch ein redraw
}

/****************************************************************************/

bool TableColumn::isEditing() const
{
    return impl->editData != NULL;
}

/****************************************************************************/

bool TableColumn::isEnabled() const
{
    return impl->enabled;
}

/****************************************************************************/

/** Write edited data to the process.
 */
void TableColumn::commit()
{
    if (!impl->editData or !impl->subscription
            or impl->subscription->getVariable().empty()) {
        return;
    }

    PdCom::Variable pv(impl->subscription->getVariable());
    auto nelem(pv.getSizeInfo().totalElements());

    for (size_t i = 0; i < nelem; i++) {
        if (impl->scale) {
            impl->editData[i] =
                (impl->editData[i] - impl->offset) / impl->scale;
        }
        else {
            impl->editData[i] = 0.0;
        }
    }
    pv.setValue(impl->editData,
            PdCom::details::TypeInfoTraits<double>::type_info.type, nelem);

    delete [] impl->editData;
    impl->editData = NULL;
    emit valueChanged();
}

/****************************************************************************/

/** Reverts all edited values.
 */
void TableColumn::revert()
{
    if (impl->editData) {
        delete [] impl->editData;
        impl->editData = NULL;
        emit valueChanged();
    }
}

/****************************************************************************/

void TableColumn::setHighlightRow(int value)
{
    impl->highlightRow = value;
}

/****************************************************************************/

void TableColumn::setHighlightColor(QColor hc)
{
    impl->highlightColor = hc;
    emit valueChanged();
}

/****************************************************************************/

void TableColumn::setDisabledColor(QColor dc)
{
    impl->disabledColor = dc;
    emit valueChanged();
}

/****************************************************************************/
