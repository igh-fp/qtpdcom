/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageModel.h"
using QtPdCom::MessageModel;

#include "MessageImpl.h"
#include "MessageItem.h"
#include "MessageManager.h"
#include "MessageModelImpl.h"
#include "Process.h"

#include <QEvent>
#include <QFile>
#include <QDomDocument>

/****************************************************************************/

/** Constructor.
 */
MessageModel::MessageModel(QObject *parent):
    QAbstractTableModel(parent),
    impl(std::unique_ptr<Impl>(new MessageModel::Impl(this)))
{
}

/****************************************************************************/

/** Destructor.
 */
MessageModel::~MessageModel()
{
    clear();
}

/****************************************************************************/

/** Loads messages from an Xml file.
 */
void MessageModel::load(
        const QString &path, /**< Path to Xml file. */
        const QString &lang, /**< Language identifier. */
        const QString &pathPrefix /**< Prefix to path (with leading /). */
        )
{
    QFile file(path);
    QDomDocument doc;
    QString errorMessage;
    int errorRow, errorColumn;
    QDomElement docElem;

    if (!file.open(QIODevice::ReadOnly)) {
        throw Exception(
                QtPdCom::MessageModel::tr("Failed to open %1.")
                .arg(file.fileName()));
    }

    if (!doc.setContent(&file, &errorMessage, &errorRow, &errorColumn)) {
        throw Exception(
                QtPdCom::MessageModel::tr("Failed to parse %1, line %2,"
                    " column %3: %4")
                .arg(file.fileName())
                .arg(errorRow).arg(errorColumn).arg(errorMessage));
    }
    file.close();

    docElem = doc.documentElement();

    if (docElem.tagName() != "EtherLabPlainMessages") {
        throw Exception(
                QtPdCom::MessageModel::tr("Failed to process %1:"
                    " No plain message file (%2)!"));
    }

    QDomNodeList children = docElem.childNodes();
    QDomNode node;
    QDomElement child;

    for (int i = 0; i < children.size(); i++) {
        node = children.item(i);
        if (node.isElement()) {
            child = node.toElement();
            if (child.tagName() == "Message") {
                try {
                    QString path = Message::Impl::pathFromPlainXmlElement(
                            child, pathPrefix);
                    int index = Message::Impl::indexFromPlainXmlElement(
                            child);

                    Message*& msg = impl->messageMap[path][index];
                    if (!msg)
                        msg = new Message();
                    msg->impl->fromPlainXmlElement(child, pathPrefix);
                    QObject::connect(msg, SIGNAL(stateChanged()),
                            impl.get(), SLOT(stateChanged()));
                } catch (Message::Exception &e) {
                    qWarning() << e.msg;
                }
            }
        }
    }

    impl->lang = lang;
}

/****************************************************************************/

/** Clears the messages.
 */
void MessageModel::clear()
{
    if (impl->announcedMessageItem) {
        impl->announcedMessageItem = nullptr;
        emit currentMessage(nullptr);
    }

    if (impl->messageItemList.count()) {
        beginRemoveRows(QModelIndex(), 0, impl->messageItemList.count() - 1);
        qDeleteAll(impl->messageItemList);
        impl->messageItemList.clear();
        endRemoveRows();
    }

    for (auto& h: impl->messageMap) {
        qDeleteAll(h);
    }

    impl->messageMap.clear();
}

/****************************************************************************/

/** Sets the maximum number of rows to fetch.
 */
void MessageModel::setRowLimit(int limit /**< Maximum number of rows. */)
{
    impl->rowLimit = limit;
}

/****************************************************************************/

/** Gets the maxium number of rows to fetch.
 */
int MessageModel::getRowLimit() const
{
    return impl->rowLimit;
}

/****************************************************************************/

/** Connects messages to the given process.
 */
void MessageModel::connect(
        QtPdCom::Process *process /**< PdCom process. */
        )
{
    impl->canFetchMore = false;
    impl->historicSeqNo = 0;

    impl->messageManager =
        dynamic_cast<MessageManager *>(process->getMessageManager());
    if (impl->messageManager) {
        QObject::connect(impl->messageManager,
                SIGNAL(processMessageSignal(PdCom::Message)),
                impl.get(), SLOT(processMessage(PdCom::Message)));
        QObject::connect(impl->messageManager,
                SIGNAL(getMessageReplySignal(PdCom::Message)),
                impl.get(), SLOT(getMessageReply(PdCom::Message)));
        QObject::connect(impl->messageManager,
                SIGNAL(activeMessagesReplySignal(
                        std::vector<PdCom::Message>)),
                impl.get(),
                SLOT(activeMessagesReply(std::vector<PdCom::Message>)));
        QObject::connect(impl->messageManager,
                SIGNAL(processResetSignal()),
                impl.get(),
                SLOT(processReset()));
    }
    else {
        qWarning() <<
            QtPdCom::MessageModel::tr("Failed to connect to message manager.");
    }

    for (auto h : impl->messageMap) {
        Impl::MessageHash::iterator i;
        for (i = h.begin(); i != h.end(); ++i) {
            if (i.key() != -1) { /** \fixme vector messages */
                continue;
            }
            Message *msg = i.value();
            PdCom::Selector selector;

            if (msg->getIndex() > -1) {
                selector = PdCom::ScalarSelector({msg->getIndex()});
            }
#if PD_DEBUG_MESSAGE_MODEL
            qDebug() << "Subscribing to" << msg->getPath() << msg->getIndex();
#endif
            try {
                msg->impl->variable.setVariable(process, msg->getPath(),
                        selector);
            } catch (AbstractScalarVariable::Exception &e) {
                qWarning() <<
                    QtPdCom::MessageModel::tr("Failed to subscribe to %1: %2")
                    .arg(msg->getPath())
                    .arg(e.msg);
            }
        }
    }
}

/****************************************************************************/

/** Sets a new language and notifies views.
 */
void MessageModel::translate(const QString &lang)
{
    impl->lang = lang;

    for (int i = 0; i < impl->messageItemList.count(); i++) {
        QModelIndex idx = index(i, 0); // only text column
        emit dataChanged(idx, idx);
    }

    if (impl->announcedMessageItem) {
        emit currentMessage(impl->announcedMessageItem->message);
    }
}

/****************************************************************************/

/** Sets an icon for a specific message type.
 */
void MessageModel::setIcon(
        Message::Type type,
        const QIcon &icon
        )
{
    impl->iconHash[type] = icon;
}

/****************************************************************************/

/** \return Icon for given message type.
 */
const QIcon &MessageModel::getIcon(
        Message::Type type
        ) const
{
    return impl->iconHash[type];
}

/****************************************************************************/

/** Sets the path (url) to an icon for a specific message type.
    If the model is used with QML views this is the only way
    to store the icon information.
 */
void MessageModel::setIconPath(
        Message::Type type,
        const QString &iconPath
        )
{
    impl->iconPathHash[type] = iconPath;
    // and also add it to the iconHash
    setIcon(type, QIcon(iconPath));
    /* vice versa would be nice, but does not work because
       to a QIcon the Path is not known */
}

/****************************************************************************/

/** Implements the model interface.
 *
 * \returns Number of active messages.
 */
int MessageModel::rowCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return impl->messageItemList.count();
    }
    else {
        return 0;
    }
}

/****************************************************************************/

/** Implements the model interface.
 *
 * \returns Number of columns.
 */
int MessageModel::columnCount(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return 3;
}

/****************************************************************************/

/** Implements the Model interface.
 */
QVariant MessageModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    auto msgItem = impl->messageItemList[index.row()];

    switch (index.column()) {
        case 0: // text
            switch (role) {
                case Qt::DisplayRole:
                    return msgItem->getText(impl->lang);
                case Qt::DecorationRole:
                    return impl->iconHash[msgItem->getType()];
                case DecorationPathRole:
                    return impl->iconPathHash[msgItem->getType()];
                case TimeStringRole:
                    // it is necessary to return other columns under
                    // column 0 because in QML a View can't address 
                    // from column but only from roles
                    return msgItem->getTimeString();
                case ResetTimeStringRole:
                    return msgItem->getResetTimeString();
	        case MessageTypeRole:
		    return msgItem->getType();
                case Qt::ToolTipRole:
                    return impl->wrapText(
                            msgItem->getDescription(impl->lang));
                default:
                    return QVariant();
            }
            break;

        case 1: // set time
            switch (role) {
                case Qt::DisplayRole:
                    return msgItem->getTimeString();
                case Qt::UserRole + 1:
                    return "";
                default:
                    return QVariant();
            }
            break;

        case 2: // reset time
            switch (role) {
                case Qt::DisplayRole:
                    return msgItem->getResetTimeString();
                case Qt::UserRole + 1:
                    return "";
                default:
                    return QVariant();
            }
            break;

        default:
            return QVariant();
    }
}

/****************************************************************************/

/** Implements the Model interface.
 */
QVariant MessageModel::headerData(
        int section,
        Qt::Orientation o,
        int role
        ) const
{
    if (role == Qt::DisplayRole && o == Qt::Horizontal) {
        switch (section) {
            case 0:
                return QtPdCom::MessageModel::tr("Message");

            case 1:
                return QtPdCom::MessageModel::tr("Time");

            case 2:
                return QtPdCom::MessageModel::tr("Reset");

            default:
                return QVariant();
        }
    }
    else {
        return QVariant();
    }
}

/****************************************************************************/

/** Implements the Model interface.
 */
Qt::ItemFlags MessageModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return Qt::ItemFlags();
    }

    auto msgItem{impl->messageItemList[index.row()]};
    return msgItem->isActive() ? Qt::ItemIsEnabled : Qt::ItemFlags();
}

/****************************************************************************/

/** Additional Rolename for decoration for use in QML views
 */

QHash<int, QByteArray> MessageModel::roleNames() const
{
    // default role names
    QHash<int, QByteArray> roles = QAbstractTableModel::roleNames();

    // extended role names
    roles[DecorationPathRole] = "decorationPath";
    roles[TimeStringRole] = "timeString";
    roles[ResetTimeStringRole] = "resetTimeString";
    roles[MessageTypeRole] = "messageTyp";
    return roles;
}

/****************************************************************************/

void MessageModel::fetchMore(const QModelIndex &parent)
{
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << parent;
#endif
    if (canFetchMore(parent)) {
#if PD_DEBUG_MESSAGE_MODEL
        qDebug() << __func__ << parent << "fetching";
#endif
        impl->getHistoryMessage();
    }
}

/****************************************************************************/

bool MessageModel::canFetchMore(const QModelIndex &parent) const
{
    bool ret = !parent.isValid()
        and impl->canFetchMore
        and impl->historicSeqNo
        and impl->messageItemList.count() < impl->rowLimit;
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << parent
        << "can" << impl->canFetchMore
        << "hist" << impl->historicSeqNo
        << "space" << (impl->messageItemList.count() < impl->rowLimit)
        << "ret" << ret;
#endif
    return ret;
}

/****************************************************************************/

/** Event handler.
 */
bool MessageModel::event(
        QEvent *event /**< Paint event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        emit headerDataChanged(Qt::Horizontal, 0, 1);
    }

    return QAbstractTableModel::event(event);
}

/****************************************************************************/
