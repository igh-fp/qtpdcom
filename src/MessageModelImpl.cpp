/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageModelImpl.h"

#include "MessageImpl.h"
#include "MessageItem.h"

#include <QDateTime>

using QtPdCom::MessageModel;

/****************************************************************************/

/** Constructor.
 */
MessageModel::Impl::Impl(MessageModel *model):
    parent{model},
    announcedMessageItem{nullptr},
    messageManager{nullptr},
    rowLimit{1000},
    canFetchMore{false},
    historicSeqNo{0},
    lessThan{MessageItem::levelNoLessThan}
{
}

/****************************************************************************/

/** Destructor.
 */
MessageModel::Impl::~Impl()
{
}

/****************************************************************************/

/** Insert a message item.
 */
void MessageModel::Impl::insertItem(MessageItem *msgItem)
{
    int row = messageItemList.indexOf(msgItem);

    if (row >= 0) {
        parent->beginRemoveRows(QModelIndex(), row, row);
        messageItemList.removeAt(row);
        parent->endRemoveRows();
    }

    row = (messageItemList.empty()
            or MessageItem::lessThan(msgItem, messageItemList.first()))
        ? 0
        : (std::lower_bound(
                    messageItemList.begin(), messageItemList.end(),
                    msgItem, lessThan)
                - messageItemList.begin());

    parent->beginInsertRows(QModelIndex(), row, row);
    messageItemList.insert(row, msgItem);
    parent->endInsertRows();

    // check announced message
    if (msgItem->isActive()
            and (!announcedMessageItem
                or msgItem->getType() < announcedMessageItem->getType()
                or (msgItem->getType() == announcedMessageItem->getType()
                    and seqNoLessThan(
                        msgItem->seqNo, announcedMessageItem->seqNo)))) {
        announcedMessageItem = msgItem;
        emit parent->currentMessage(announcedMessageItem->message);
    }
    else if (msgItem->resetTime and msgItem == announcedMessageItem) {
        announcedMessageItem = nullptr;

        MessageItemList sortedList(messageItemList);
        std::sort(sortedList.begin(), sortedList.end(),
                MessageItem::levelNoLessThan);

        auto type = sortedList.front()->getType();
        for (MessageItemList::const_iterator it = sortedList.begin();
                (it != sortedList.end()
                 and !(*it)->resetTime
                 and (*it)->getType() == type);
                ++it) {
            announcedMessageItem = *it;
        }

        emit parent->currentMessage(announcedMessageItem->message);
    }
}

/****************************************************************************/

/** Called from the PdCom interface, if a new message appears via
 * processMessage() or in context of activeMessagesReply().
 */
void MessageModel::Impl::addProcessMessage(
        const PdCom::Message &pdComMsg
        )
{
    QString path{QString::fromStdString(pdComMsg.path)};
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << pdComMsg.seqNo;
#endif

    Message*& msg = messageMap[path][pdComMsg.index];
    if (!msg) {
        msg = new Message();
        msg->impl->fromPdComMessage(pdComMsg);
    }

    auto msgItem{msg->impl->currentItem};
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << "msgItem" << msgItem;
    if (msgItem) {
        qDebug() << __func__ << "currentItem seqNo" << msgItem->seqNo;
    }
#endif

    if (pdComMsg.level != PdCom::LogLevel::Reset) { // set
        if (msgItem) {
            // already has current item
            if (!msgItem->seqNo) {
                // Current item has zero seqNo -> from mixed mode.
                // Just attach the seqNo.
                msgItem->seqNo = pdComMsg.seqNo;
            }
            else if (msgItem->seqNo != pdComMsg.seqNo) {
                // Current item has different seqNo.
                // Not ours, thus create a new one.
                msgItem = new MessageItem(msg, this, pdComMsg.time.count());
                msgItem->seqNo = pdComMsg.seqNo;
            }
        }
        else { // no current item
            msgItem = new MessageItem(msg, this, pdComMsg.time.count());
            msgItem->seqNo = pdComMsg.seqNo;
            msg->impl->currentItem = msgItem;
        }
        insertItem(msgItem);
    }
    else { // reset
        if (msgItem) {
            msgItem->resetTime = pdComMsg.time.count();

            // notify views
            int row = messageItemList.indexOf(msgItem);
#if PD_DEBUG_MESSAGE_MODEL
            qDebug() << __func__ << "reset row" << row;
#endif
            if (row >= 0) {
                QModelIndex idx0 = parent->index(row, 0);
                QModelIndex idx1 = parent->index(row, 2);
                emit parent->dataChanged(idx0, idx1);
            }
            msg->impl->currentItem = nullptr;
        }
    }

    emit parent->anyMessage(msg);
}

/****************************************************************************/

/** Called from the PdCom interface, if a historic message appears via
 * getMessageReply().
 */
void MessageModel::Impl::addHistoricMessage(
        const PdCom::Message &pdComMsg,
        const PdCom::Message &resetMsg
        )
{
    QString path{QString::fromStdString(pdComMsg.path)};
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << pdComMsg.seqNo << resetMsg.seqNo;
#endif

    Message*& msg = messageMap[path][pdComMsg.index];
    if (!msg) {
        msg = new Message();
        msg->impl->fromPdComMessage(pdComMsg);
    }

    auto msgItem = new MessageItem(msg, this, pdComMsg.time.count());
    msgItem->seqNo = pdComMsg.seqNo;
    msgItem->resetTime = resetMsg.time.count();
    insertItem(msgItem);
}

/****************************************************************************/

/** Returns a wrapped version of a string.
 */
QString MessageModel::Impl::wrapText(const QString &text, unsigned int width)
{
    QString ret;
    int lineOffset, i;

    lineOffset = 0;
    while (lineOffset + (int) width < text.length()) {
        // search last space before line end
        for (i = width; i >= 0; i--) {
            if (text[lineOffset + i].isSpace())
                break; // break at whitespace
        }
        if (i < 0) // no whitespace found
            i = width; // "hard" break at line end

        ret += text.mid(lineOffset, i) + QChar(QChar::LineSeparator);
        lineOffset += i + 1; // skip line and whitespace
    }

    ret += text.mid(lineOffset); // append remaining string
    return ret;
}

/****************************************************************************/

void MessageModel::Impl::getHistoryMessage()
{
    if (!messageManager) {
        qWarning() << __func__ << "no message manager";
        return;
    }

#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << "setting canFetchMore to false";
#endif
    canFetchMore = false; // avoid fetchMore called twice for same seqNo

    uint32_t prevSeqNo = historicSeqNo - 1;
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << "fetching" << prevSeqNo;
#endif

    try {
        messageManager->getMessage(prevSeqNo);
    }
    catch (PdCom::Exception &e) {
        qDebug() << __func__ << e.what();
    }
}

/*****************************************************************************
 * private slots
 ****************************************************************************/

/** Reacts on process values changes of all messages to watch.
 */
void MessageModel::Impl::stateChanged()
{
    Message *msg = (Message *) sender();
    DoubleVariable &var = msg->impl->variable;
    double time{var.hasData() ? var.getValue() : 0.0};

#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << msg->getPath() << msg->getIndex() << time;
#endif

    emit parent->anyMessage(msg);

    MessageItem *msgItem{nullptr};

    if (time) { // set
        MessageItem *msgItem{msg->impl->currentItem};
        if (!msgItem) {
            msgItem = new MessageItem(msg, this, time * 1e9);
            msg->impl->currentItem = msgItem;
            insertItem(msgItem);
        }
    }
    else { // reset
        msgItem = msg->impl->currentItem;
        if (msgItem) {
            auto now{QDateTime::currentDateTime()};
            msgItem->resetTime = now.toMSecsSinceEpoch() * 1000000U;

            // notify views
            int row = messageItemList.indexOf(msgItem);
#if PD_DEBUG_MESSAGE_MODEL
            qDebug() << __func__ << "reset row" << row;
#endif
            if (row >= 0) {
                QModelIndex idx0 = parent->index(row, 0);
                QModelIndex idx1 = parent->index(row, 2);
                emit parent->dataChanged(idx0, idx1);
            }

            msg->impl->currentItem = nullptr;
        }
    }
}

/****************************************************************************/

void MessageModel::Impl::processMessage(PdCom::Message message)
{
#if PD_DEBUG_MESSAGE_MODEL
    auto path = QString::fromStdString(message.path);
    auto text = QString::fromStdString(message.text);
    qDebug() << __func__;
    qDebug()
        << "seqNo" << message.seqNo
        << "level" << (int) message.level
        << "path" << path
        << "time" << message.time.count()
        << "index" << message.index
        << "text" << text;
#endif

    addProcessMessage(message);
}

/****************************************************************************/

void MessageModel::Impl::getMessageReply(PdCom::Message message)
{
    auto path = QString::fromStdString(message.path);

#if PD_DEBUG_MESSAGE_MODEL
    auto text = QString::fromStdString(message.text);
    qDebug() << __func__;
    qDebug()
        << "seqNo" << message.seqNo
        << "level" << (int) message.level
        << "path" << path
        << "time" << message.time.count()
        << "index" << message.index
        << "text" << text;
#endif

    if (path.isEmpty()) {
        // EOF marker - no more messages from process
        return;
    }

    historicSeqNo = message.seqNo;

    canFetchMore = message.level != PdCom::LogLevel::Reset;
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__ << "setting canFetchMore to" << canFetchMore;
#endif

    bool stillActive{false};
    if (canFetchMore) {
        // found a message that was set in the past. try to find the reset.
        bool found{false};
#if PD_DEBUG_MESSAGE_MODEL
        qDebug() << __func__ << "reset msg list size is"
            << resetMessagesList.size();
#endif
        for (auto r = resetMessagesList.begin();
                r != resetMessagesList.end(); r++) {
            if (r->path == message.path and r->index == message.index) {
                addHistoricMessage(message, *r);
                resetMessagesList.erase(r);
#if PD_DEBUG_MESSAGE_MODEL
                found = true;
#endif
                break;
            }
        }
#if PD_DEBUG_MESSAGE_MODEL
        if (!found) {
            qDebug() << __func__ <<
                "reset message not found for" <<  message.seqNo;
            // found a message that seems to be still active. Go on with
            // reading, otherwise views won't ask for more data
            stillActive = true;
        }
#endif
    }
    else {
        resetMessagesList.append(message);
    }

    if ((!canFetchMore or stillActive) and historicSeqNo) {
        getHistoryMessage();
    }
}

/****************************************************************************/

void MessageModel::Impl::activeMessagesReply(
        std::vector<PdCom::Message> messageList)
{
    quint32 maxSeqNo{0};

#if PD_DEBUG_MESSAGE_MODEL
    qDebug().nospace() << __func__ << "(" << messageList.size() << ")";
#endif
    for (auto message : messageList) {
#if PD_DEBUG_MESSAGE_MODEL
        auto path = QString::fromStdString(message.path);
        auto text = QString::fromStdString(message.text);
        qDebug()
            << "seqNo" << message.seqNo
            << "level" << (int) message.level
            << "path" << path
            << "time" << message.time.count()
            << "index" << message.index
            << "text" << text;
#endif
        if (message.level != PdCom::LogLevel::Reset) {
            addProcessMessage(message);
        }
        else {
            // one entry in the list of active messages can be a reset
            // message, to announce the current sequence number
            resetMessagesList.append(message);
        }

        if (message.seqNo > maxSeqNo) {
            maxSeqNo = message.seqNo;
        }
    }

    if (maxSeqNo > 0) {
        // now fetch one historic message
        historicSeqNo = maxSeqNo;
        getHistoryMessage();
    }
}

/****************************************************************************/

void MessageModel::Impl::processReset()
{
#if PD_DEBUG_MESSAGE_MODEL
    qDebug() << __func__;
#endif

    canFetchMore = false;
    historicSeqNo = 0;
    resetMessagesList.clear();

    parent->beginResetModel();
    messageItemList.clear();

    // reset current item pointers
    for (auto hash : messageMap) {
        for (auto msg : hash) {
            if (msg->impl->currentItem) {
                msg->impl->currentItem = nullptr;
            }
        }
    }
    parent->endResetModel();

    if (messageManager) {
        QObject::disconnect(messageManager,
                SIGNAL(processMessageSignal(PdCom::Message)),
                this, SLOT(processMessage(PdCom::Message)));
        QObject::disconnect(messageManager,
                SIGNAL(getMessageReplySignal(PdCom::Message)),
                this, SLOT(getMessageReply(PdCom::Message)));
        QObject::disconnect(messageManager,
                SIGNAL(activeMessagesReplySignal(
                        std::vector<PdCom::Message>)),
                this,
                SLOT(activeMessagesReply(std::vector<PdCom::Message>)));
        QObject::disconnect(messageManager,
                SIGNAL(processResetSignal()),
                this,
                SLOT(processReset()));
    }
}

/****************************************************************************/
