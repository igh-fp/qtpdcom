/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_TABLEMODEL_IMPL_H
#define QTPDCOM_TABLEMODEL_IMPL_H

#include <QtPdCom1/TableModel.h>

#include <QAbstractTableModel>
#include <QVector>
#include <QColor>

namespace QtPdCom {

/****************************************************************************/

/** Table model.
 *
 * \see TableColumn.
 */
class TableModel::Impl
{
    public:
        Impl(TableModel *parent) : parent(parent) {}

    private:
        TableModel * const parent;
        unsigned int rows = 0;
        unsigned int visibleRows = UINT_MAX;
        unsigned int rowCapacity = 0;
        typedef QVector<TableColumn *> ColumnVector; /**< Column vector type.
                                                      */
        ColumnVector columnVector; /**< Vector of table columns. */

        void updateRows();
        QtPdCom::IntVariable valueHighlightRow;
        QtPdCom::IntVariable visibleRowCount;

        friend class TableModel;

};

/****************************************************************************/

} // namespace QTPDCOM_TABLEMODEL_IMPL_H

#endif
