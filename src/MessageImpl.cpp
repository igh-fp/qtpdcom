/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageImpl.h"
#include "MessageItem.h"
using namespace QtPdCom;

#include <QDateTime>

/****************************************************************************/

/** Constructor.
 */
Message::Impl::Impl(Message *message):
    parent{message},
    type{Information},
    index{-1},
    currentItem{nullptr}
{
    QObject::connect(&variable, SIGNAL(valueChanged()),
            this, SLOT(valueChanged()));
}

/****************************************************************************/

/** Destructor.
 */
Message::Impl::~Impl()
{
}

/****************************************************************************/

/** Get the path.
 */
QString Message::Impl::pathFromPlainXmlElement(
        QDomElement elem, /**< Element. */
        const QString &pathPrefix /**< Prefix to path (with leading /). */
        )
{
    if (!elem.hasAttribute("variable")) {
        throw Exception("Messages has no variable attribute!");
    }

    return pathPrefix + elem.attribute("variable");
}

/****************************************************************************/

/** Get the index.
 */
int Message::Impl::indexFromPlainXmlElement(
        QDomElement elem /**< Element. */
        )
{
    if (not elem.hasAttribute("index")) {
        return -1;
    }

    bool ok{false};
    int ret = elem.attribute("index").toInt(&ok);
    if (not ok or ret < -1) {
        throw Exception("Message has invalid index value!");
    }

    return ret;
}

/****************************************************************************/

/** Constructor with XML element.
 */
void Message::Impl::fromPlainXmlElement(
        QDomElement elem, /**< Element. */
        const QString &pathPrefix /**< Prefix to path (with leading /). */
        )
{
    QDomNodeList children = elem.childNodes();

    path = pathFromPlainXmlElement(elem, pathPrefix);
    index = indexFromPlainXmlElement(elem);

    if (!elem.hasAttribute("type")) {
        throw Exception("Messages has no type attribute!");
    }

    type = typeFromString(elem.attribute("type"));

    // find Text and Descriptions elements
    for (int i = 0; i < children.size(); i++) {
        QDomNode node = children.item(i);
        if (node.isElement()) {
            QDomElement child = node.toElement();
            if (child.tagName() == "Text") {
                loadTranslations(child, text);
            }
            else if (child.tagName() == "Description") {
                loadTranslations(child, description);
            }
        }
    }
}

/****************************************************************************/

/** Constructor with PdCom5 message.
 */
void Message::Impl::fromPdComMessage(
        const PdCom::Message &message
        )
{
    switch (message.level) {
        case PdCom::LogLevel::Reset:
            type = Information;
            break;
        case PdCom::LogLevel::Emergency:
            type = Error;
            break;
        case PdCom::LogLevel::Alert:
            type = Error;
            break;
        case PdCom::LogLevel::Critical:
            type = Error;
            break;
        case PdCom::LogLevel::Error:
            type = Error;
            break;
        case PdCom::LogLevel::Warn:
            type = Warning;
            break;
        case PdCom::LogLevel::Info:
            type = Information;
            break;
        case PdCom::LogLevel::Debug:
            type = Information;
            break;
        case PdCom::LogLevel::Trace:
            type = Information;
            break;
    }

    path = message.path.c_str();
    index = message.index;
    text[""] = message.text.c_str();
}

/****************************************************************************/

/** Returns the message time as a string.
 */
QString Message::Impl::timeString(quint64 time_ns)
{
    quint64 sec, nsec, usec;
    QDateTime dt;
    QString usecStr;

    sec = time_ns / 1000000000U;
    nsec = time_ns % 1000000000U;
    usec = nsec / 1000U;
    dt.setTime_t(sec);
    usecStr = QString(",%1").arg(usec, 6, 10, QLatin1Char('0'));
    return dt.toString("yyyy-MM-dd hh:mm:ss") + usecStr;
}

/****************************************************************************/

/** Processes a TextNode XML element.
 */
void Message::Impl::loadTranslations(
        QDomElement elem, /**< Element. */
        TranslationMap &map /**< Translation map. */
        )
{
    QDomNodeList children = elem.childNodes();

    for (int i = 0; i < children.size(); i++) {
        QDomNode node = children.item(i);
        if (!node.isElement()) {
            continue;
        }
        QDomElement child = node.toElement();
        if (child.tagName() != "Translation") {
            throw Exception(QString("Expected Translation element, got %1!")
                    .arg(child.tagName()));
        }
        if (!child.hasAttribute("lang")) {
            throw Exception("Translation missing lang attribute!");
        }
        map[child.attribute("lang")] = child.text().simplified();
    }
}

/****************************************************************************/

/** Converts a message type string to the appropriate #Type.
 */
Message::Type Message::Impl::typeFromString(const QString &str)
{
    if (str == "Information") {
        return Information;
    }
    if (str == "Warning") {
        return Warning;
    }
    if (str == "Error") {
        return Error;
    }
    if (str == "Critical") {
        return Critical;
    }

    throw Message::Exception(QString("Invalid message type '%1'").arg(str));
}

/****************************************************************************/

/** Variable value changed.
 */
void Message::Impl::valueChanged()
{
    emit parent->stateChanged();
}

/****************************************************************************/
