/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_MESSAGE_ITEM
#define PD_MESSAGE_ITEM

#include "MessageModelImpl.h"

#include <QObject>

/****************************************************************************/

namespace QtPdCom {

bool seqNoLessThan(uint32_t, uint32_t);

/****************************************************************************/

struct MessageModel::Impl::MessageItem
{
    MessageItem() = delete;
    MessageItem(Message *, MessageModel::Impl *, quint64);

    Message::Type getType() const;
    QString getText(const QString &) const;
    QString getDescription(const QString &) const;
    QString getTimeString() const;
    QString getResetTimeString() const;
    bool isActive() const;

    static bool seqNoLessThan(const MessageItem *, const MessageItem *);
    static bool setTimeLessThan(const MessageItem *, const MessageItem *);
    static bool levelNoLessThan(const MessageItem *, const MessageItem *);
    static bool lessThan(const MessageItem *, const MessageItem *);

    Message * const message;
    MessageModel::Impl * const modelImpl;

    quint32 seqNo;

    quint64 const setTime;
    quint64 resetTime;
};

/****************************************************************************/

}

#endif

/****************************************************************************/
