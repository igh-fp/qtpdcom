/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageItem.h"
#include "MessageImpl.h"

namespace QtPdCom {

/****************************************************************************/

bool seqNoLessThan(uint32_t a, uint32_t b)
{
    return int32_t(a - b) > 0; // greater seqNos shall be on top of the list
}

/****************************************************************************/

MessageModel::Impl::MessageItem::MessageItem(
        Message *message,
        MessageModel::Impl *modelImpl,
        quint64 setTime):
    message{message},
    modelImpl{modelImpl},
    seqNo{0},
    setTime{setTime},
    resetTime{0}
{
}

/****************************************************************************/

Message::Type MessageModel::Impl::MessageItem::getType() const
{
    return message->getType();
}

/****************************************************************************/

QString MessageModel::Impl::MessageItem::getText(
        const QString &lang) const
{
    return message->getText(lang);
}

/****************************************************************************/

QString MessageModel::Impl::MessageItem::getDescription(
        const QString &lang) const
{
    return message->getDescription(lang);
}

/****************************************************************************/

QString MessageModel::Impl::MessageItem::getTimeString() const
{
    return message->impl->timeString(setTime);
}

/****************************************************************************/

QString MessageModel::Impl::MessageItem::getResetTimeString() const
{
    if (resetTime) {
        return message->impl->timeString(resetTime);
    }
    else {
        return QString();
    }
}

/****************************************************************************/

bool MessageModel::Impl::MessageItem::isActive() const
{
    return resetTime == 0;
}

/****************************************************************************/

bool MessageModel::Impl::MessageItem::seqNoLessThan(const MessageItem *a,
        const MessageItem *b)
{
    return QtPdCom::seqNoLessThan(a->seqNo, b->seqNo);
}

/****************************************************************************/

bool MessageModel::Impl::MessageItem::setTimeLessThan(const MessageItem *a,
        const MessageItem *b)
{
    qint64 diff = a->setTime - b->setTime;
    return diff > 0
        or (!diff and seqNoLessThan(a, b));
}

/****************************************************************************/

bool MessageModel::Impl::MessageItem::levelNoLessThan(
        const MessageModel::Impl::MessageItem *a, const MessageItem *b)
{
    bool aIsReset = a->resetTime;
    bool bIsReset = b->resetTime;

    /* sorting rules:
     *
     * - if a message is reset, it should be displayed below all active
     *   messages.
     * - among reset messages, the sorting should happen just accorting the
     *   time (and sequence number, if time is equal), so ignoring the type
     *   for reset messages.
     * - for active messages, those with more critical types shall be
     *   displayed on top of the list, thus the set time shall be as ordering
     *   citerion for active messages with the same type.
     */

    bool ret = (!aIsReset and !bIsReset
            and (a->getType() > b->getType() // critical types on top
                or (a->getType() == b->getType()
                    and setTimeLessThan(a, b))))
        or (bIsReset
                and (!aIsReset or seqNoLessThan(a, b)));
    return ret;
}

/****************************************************************************/

bool MessageModel::Impl::MessageItem::lessThan(const MessageItem *a,
        const MessageItem *b)
{
    return a->modelImpl->lessThan(a, b);
}

/****************************************************************************/

} // namespace

/****************************************************************************/
