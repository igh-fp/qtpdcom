/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_MESSAGE_MODEL_IMPL
#define QTPDCOM_MESSAGE_MODEL_IMPL

#include "MessageModel.h"
#include "MessageManager.h"

#include <pdcom5/MessageManagerBase.h>

#include <QObject>
#include <QSet>

/****************************************************************************/

namespace QtPdCom {

/****************************************************************************/

class MessageModel::Impl:
    public QObject
{
    Q_OBJECT

    friend class MessageModel;
    friend class Message;

    public:
        Impl(MessageModel *);
        ~Impl();

        struct MessageItem;

        void insertItem(MessageItem *);
        void addProcessMessage(const PdCom::Message &);
        void addHistoricMessage(const PdCom::Message &,
                const PdCom::Message &);

        static QString wrapText(const QString &, unsigned int = 78);

    private:
        MessageModel * const parent;

        // Map of message[path][index]
        typedef QHash<int, Message *> MessageHash;
        typedef QMap<QString, MessageHash> MessageMap;
        MessageMap messageMap;

        typedef QList<MessageItem *> MessageItemList;
        MessageItemList messageItemList;

        typedef QHash<Message::Type, QIcon> IconHash; /**< Icon hash
                                                            type. */
        IconHash iconHash; /**< Icons for message types.
                             \see Message::Type. */

        /** Icon hash type (with path). */
        typedef QHash<Message::Type, QString> IconPathHash;
         /** Icons for message types (with path). \see Message::Type. */
        IconPathHash iconPathHash;

        MessageItem *announcedMessageItem; /**< Recently announced message. */
        QString lang;
        MessageManager *messageManager;
        int rowLimit;
        bool canFetchMore;
        uint32_t historicSeqNo;

        void getHistoryMessage();

        bool (*lessThan)(const MessageItem *, const MessageItem *);

        /** list of history messages that were reset */
        QList<PdCom::Message> resetMessagesList;

    private slots:
        void stateChanged();
        void processMessage(PdCom::Message message);
        void getMessageReply(PdCom::Message message);
        void activeMessagesReply(std::vector<PdCom::Message>);
        void processReset();
};

/****************************************************************************/

}

#endif

/****************************************************************************/
