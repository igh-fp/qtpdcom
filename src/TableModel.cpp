/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableModel.h"
#include "TableModelImpl.h"

using QtPdCom::TableModel;

#include <QDebug>

/****************************************************************************/

/** Constructor.
 */
TableModel::TableModel(QObject *parent)
    : QAbstractTableModel(parent),
    impl(new TableModel::Impl(this))
{
    connect(&impl->valueHighlightRow, SIGNAL(valueChanged()),
            this, SLOT(highlightRowChanged()));
    connect(&impl->visibleRowCount, SIGNAL(valueChanged()),
            this, SLOT(visibleRowCountChanged()));
}

/****************************************************************************/

/** Destructor.
 */
TableModel::~TableModel()
{
    impl->valueHighlightRow.clearVariable();
    clearColumns();
}

/****************************************************************************/

/** Adds a column.
 */
void TableModel::addColumn(TableColumn *col)
{
    beginInsertColumns(QModelIndex(),
            impl->columnVector.count(), impl->columnVector.count());
    impl->columnVector.append(col);
    endInsertColumns();

    QObject::connect(col, SIGNAL(dimensionChanged()),
            this, SLOT(dimensionChanged()));
    QObject::connect(col, SIGNAL(headerChanged()),
            this, SLOT(columnHeaderChanged()));
    QObject::connect(col, SIGNAL(valueChanged()),
            this, SLOT(valueChanged()));

    impl->updateRows();
}

/****************************************************************************/

/** Clears the Columns.
 */
void TableModel::clearColumns()
{
    beginRemoveColumns(QModelIndex(), 0, impl->columnVector.count() - 1);
    impl->columnVector.clear();
    endRemoveColumns();

    for (const auto column : impl->columnVector) {
        QObject::disconnect(column, SIGNAL(dimensionChanged()),
                this, SLOT(dimensionChanged()));
        QObject::disconnect(column, SIGNAL(headerChanged()),
                this, SLOT(columnHeaderChanged()));
        QObject::disconnect(column, SIGNAL(valueChanged()),
                this, SLOT(valueChanged()));
    }

    impl->updateRows();
}

/****************************************************************************/

bool TableModel::isEditing() const
{
    bool editing = false;

    for (const auto column : impl->columnVector) {
        if (column->isEditing()) {
            editing = true;
            break;
        }
    }

    return editing;
}

/****************************************************************************/

unsigned int TableModel::getRowCapacity() const
{
  return impl->rowCapacity;
}

/****************************************************************************/

bool TableModel::hasVisibleRowsVariable() const
{
  return impl->visibleRowCount.hasVariable();
}

/****************************************************************************/

/** Implements the model interface.
 *
 * \returns Number of rows.
 */
int TableModel::rowCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return impl->rows;
    } else {
        return 0;
    }
}

/****************************************************************************/

/** Implements the model interface.
 *
 * \returns Number of columns.
 */
int TableModel::columnCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return impl->columnVector.count();
    } else {
        return 0;
    }
}

/****************************************************************************/

/** Implements the Model interface.
 */
QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    return impl->columnVector[index.column()]->data(index.row(), role);
}

/****************************************************************************/

/** Implements the Model interface.
 */
QVariant TableModel::headerData(
        int section,
        Qt::Orientation o,
        int role
        ) const
{
    if (o != Qt::Horizontal) {
        return QVariant();
    }

    return impl->columnVector[section]->headerData(role);
}

/****************************************************************************/

/** Implements the Model interface.
 */
Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags f;

    if (index.isValid()) {
        f = impl->columnVector[index.column()]->flags(index.row());
    }

    return f;
}

/****************************************************************************/

bool TableModel::setData(const QModelIndex &index,
        const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    bool ret = impl->columnVector[index.column()]->setData(index.row(),
            value.toString(), role);

    emit editingChanged(isEditing());

    return ret;
}

/****************************************************************************/

void TableModel::setHighlightRowVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission
        )
{
    clearHighlightRowVariable();

    if (pv.empty()) {
        return;
    }

    impl->valueHighlightRow.setVariable(pv, selector, transmission);
}

/****************************************************************************/

void TableModel::setHighlightRowVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission
        )
{
    clearHighlightRowVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->valueHighlightRow.setVariable(process, path, selector, transmission);
}

/****************************************************************************/

void TableModel::clearHighlightRowVariable()
{
    impl->valueHighlightRow.clearVariable();

    for (const auto column : impl->columnVector) {
        column->setHighlightRow(-1);
    }
}

/****************************************************************************/

void TableModel::setVisibleRowsVariable(PdCom::Variable pv)
{
    clearVisibleRowsVariable();

    if (pv.empty()) {
        return;
    }

    impl->visibleRowCount.setVariable(pv);
}

/****************************************************************************/

void TableModel::setHighlightColor(QColor hc, int idx)
{
    if (idx <= -1) {
        for (const auto column : impl->columnVector) {
            column->setHighlightColor(hc);
        }
    } else if (idx < impl->columnVector.size()) {
        impl->columnVector[idx]->setHighlightColor(hc);
    }
}

/****************************************************************************/

void TableModel::setDisabledColor(QColor dc, int idx)
{
    if (idx <= -1) {
        for (const auto column : impl->columnVector) {
            column->setDisabledColor(dc);
        }
    } else if (idx < impl->columnVector.size()) {
        impl->columnVector[idx]->setDisabledColor(dc);
    }
}

/****************************************************************************/

void TableModel::clearVisibleRowsVariable()
{
    impl->visibleRowCount.clearVariable();
    impl->visibleRows = UINT_MAX;
    impl->updateRows();
}

/****************************************************************************/

/** Commits all edited data.
 */
void TableModel::commit()
{
    for (const auto column : impl->columnVector) {
        column->commit();
    }

    emit editingChanged(false);
}

/****************************************************************************/

/** Reverts all edited data.
 */
void TableModel::revert()
{
    for (const auto column : impl->columnVector) {
        column->revert();
    }

    emit editingChanged(false);
}

/****************************************************************************/

/** updates the visibleRowCount variable.

 */
void TableModel::addRow()
{
    if (impl->rowCapacity > 0) {
       impl->visibleRowCount.writeValue(impl->visibleRows + 1);
    }
}

/****************************************************************************/

/** updates the visibleRowCount variable.

 */
void TableModel::remRow()
{
    if (impl->visibleRows > 1) {
        impl->visibleRowCount.writeValue(impl->visibleRows - 1);
    }
}

/****************************************************************************/

/** Calculates the number of table rows.
 */
void TableModel::Impl::updateRows()
{
    ColumnVector::const_iterator it;
    unsigned int maxRows = 0;

    for (it = columnVector.begin(); it != columnVector.end(); it++) {
        unsigned int r = (*it)->getRows();
        if (r > maxRows) {
            maxRows = r;
        }
    }

    if (maxRows > visibleRows) {
        rowCapacity = maxRows - visibleRows;
        maxRows = visibleRows;
    }
    else {
        rowCapacity = 0;
    }

    if (maxRows > rows) {
        parent->beginInsertRows(QModelIndex(), rows, maxRows - 1);
        rows = maxRows;
        parent->endInsertRows();
    } else if (maxRows < rows) {
        parent->beginRemoveRows(QModelIndex(), maxRows, rows - 1);
        rows = maxRows;
        parent->endRemoveRows();
    }
}

/****************************************************************************/

/** Reacts on process variable dimension changes.
 */
void TableModel::dimensionChanged()
{
    impl->updateRows();
}

/****************************************************************************/

/** Reacts on header data changes.
 */
void TableModel::columnHeaderChanged()
{
    TableColumn *col = dynamic_cast<TableColumn *>(sender());
    int j = impl->columnVector.indexOf(col);

    if (j > -1) {
        headerDataChanged(Qt::Horizontal, j, j);
    }
}

/****************************************************************************/

/** Reacts on process variable changes.
 */
void TableModel::valueChanged()
{
    TableColumn *col = dynamic_cast<TableColumn *>(sender());

    int j = impl->columnVector.indexOf(col);
    if (j > -1) {
        QModelIndex topLeft = index(0, j);
        QModelIndex bottomRight = index(qMin(col->getRows(), impl->rows) - 1, j);
        emit dataChanged(topLeft, bottomRight);
        /* qDebug() << "Table changes: " << topLeft.row()
         * << topLeft.column() << bottomRight.row()
         * << bottomRight.column(); */
    }
}

/****************************************************************************/

void TableModel::highlightRowChanged()
{
    unsigned int row = -1;

    if (impl->valueHighlightRow.hasData()) {
        row = impl->valueHighlightRow.getValue();
    }

    for (const auto column : impl->columnVector) {
        column->setHighlightRow(row);
    }

    if ((impl->columnVector.count() > 0) && (row < impl->rows)) {
        QModelIndex topLeft = index(row, 0);
        QModelIndex bottomRight = index(row, impl->columnVector.count() - 1);
        emit dataChanged(topLeft, bottomRight);
    }
}

/****************************************************************************/

void TableModel::visibleRowCountChanged()
{
    if (impl->visibleRowCount.hasData()) {
        impl->visibleRows = impl->visibleRowCount.getValue();
        impl->updateRows();
    }
}

/****************************************************************************/
