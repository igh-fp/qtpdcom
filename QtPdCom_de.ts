<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>QtPdCom::MessageModel</name>
    <message>
        <location filename="src/MessageModel.cpp" line="71"/>
        <source>Failed to open %1.</source>
        <translation>Fehler beim Öffnen von %1.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="77"/>
        <source>Failed to parse %1, line %2, column %3: %4</source>
        <translation>Fehler beim Parsen von %1, Zeile %2, Spalte %3: %4</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="88"/>
        <source>Failed to process %1: No plain message file (%2)!</source>
        <translation>Konnte %1 nicht verarbeiten, da es keine flache Meldungsdatei ist (%2)!</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="193"/>
        <source>Failed to connect to message manager.</source>
        <translation>Konnte nicht mit Meldungsverwaltung verbinden.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="385"/>
        <source>Reset</source>
        <translation>Quittiert</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="207"/>
        <source>Failed to subscribe to %1: %2</source>
        <translation>Fehler beim Abonnieren von %1: %2</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="379"/>
        <source>Message</source>
        <translation>Meldung</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="382"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
</context>
</TS>
